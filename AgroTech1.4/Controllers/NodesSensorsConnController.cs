﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgroTech1._4.Models;

namespace AgroTech1._4.Controllers
{
    public class NodesSensorsConnController : Controller
    {
        private eMES_AgroTechEntities db = new eMES_AgroTechEntities();

        // GET: NodesSensorsConn/Create
        public ActionResult Create()
        {
            ViewBag.node_id = new SelectList(db.tblATNodes, "node_id", "node_name");
            ViewBag.sensor_id = new SelectList(db.tblATSensors, "sensor_id", "sensor_type");
            return View();
        }

        // POST: NodesSensorsConn/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "id,node_id,sensor_id")] tblATNodesSensorsConnection tblATNodesSensorsConnection)
        {
            if (ModelState.IsValid)
            {
                db.tblATNodesSensorsConnections.Add(tblATNodesSensorsConnection);
                db.SaveChanges();
                return RedirectToAction("IndexNodesSensors", "NodesSensors");
            }

            ViewBag.node_id = new SelectList(db.tblATNodes, "node_id", "node_name", tblATNodesSensorsConnection.node_id);
            ViewBag.sensor_id = new SelectList(db.tblATSensors, "sensor_id", "sensor_type", tblATNodesSensorsConnection.sensor_id);
            return View(tblATNodesSensorsConnection);           
        }

        // GET: Plants/Delete/5
        public ActionResult Delete(long? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblATNodesSensorsConnection tblATNodesSensorsConnection = db.tblATNodesSensorsConnections.Find(id);
            if (tblATNodesSensorsConnection == null)
            {
                return HttpNotFound();
            }
            return View(tblATNodesSensorsConnection);
        }

        // POST: Plants/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tblATNodesSensorsConnection tblATNodesSensorsConnection = db.tblATNodesSensorsConnections.Find(id);
            db.tblATNodesSensorsConnections.Remove(tblATNodesSensorsConnection);
            db.SaveChanges();
            return RedirectToAction("IndexNodesSensors", "NodesSensors");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
