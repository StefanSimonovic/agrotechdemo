﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgroTech1._4.Models;

namespace AgroTech1._4.Controllers
{
    public class NodesController : Controller
    {
        private eMES_AgroTechEntities db = new eMES_AgroTechEntities();

        // GET: Nodes
        public ActionResult IndexNodes()
        {
            return View(db.tblATNodes.ToList());
        }

        // GET: Nodes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Nodes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "node_id,node_name,node_type")] tblATNode tblATNode)
        {
            if (ModelState.IsValid)
            {
                db.tblATNodes.Add(tblATNode);
                db.SaveChanges();
                return RedirectToAction("IndexNodes");
            }

            return View(tblATNode);
        }

        // GET: Nodes/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblATNode tblATNode = db.tblATNodes.Find(id);
            if (tblATNode == null)
            {
                return HttpNotFound();
            }
            return View(tblATNode);
        }

        // POST: Nodes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "node_id,node_name,node_type")] tblATNode tblATNode)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblATNode).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexNodes");
            }
            return View(tblATNode);
        }

        // GET: Nodes/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblATNode tblATNode = db.tblATNodes.Find(id);
            if (tblATNode == null)
            {
                return HttpNotFound();
            }
            return View(tblATNode);
        }

        // POST: Nodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tblATNode tblATNode = db.tblATNodes.Find(id);
            db.tblATNodes.Remove(tblATNode);
            db.SaveChanges();
            return RedirectToAction("IndexNodes");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
