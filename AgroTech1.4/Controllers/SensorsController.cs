﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgroTech1._4.Models;

namespace AgroTech1._4.Controllers
{
    public class SensorsController : Controller
    {
        private eMES_AgroTechEntities db = new eMES_AgroTechEntities();

        // GET: Sensors
        public ActionResult IndexSensors()
        {
            return View(db.tblATSensors.ToList());
        }

        // GET: Sensors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sensors/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "sensor_id,sensor_type,measuring_unit")] tblATSensor tblATSensor)
        {
            if (ModelState.IsValid)
            {
                db.tblATSensors.Add(tblATSensor);
                db.SaveChanges();
                return RedirectToAction("IndexSensors");
            }

            return View(tblATSensor);
        }

        // GET: Sensors/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblATSensor tblATSensor = db.tblATSensors.Find(id);
            if (tblATSensor == null)
            {
                return HttpNotFound();
            }
            return View(tblATSensor);
        }

        // POST: Sensors/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "sensor_id,sensor_type,measuring_unit")] tblATSensor tblATSensor)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblATSensor).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("IndexSensors");
            }
            return View(tblATSensor);
        }

        // GET: Sensors/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblATSensor tblATSensor = db.tblATSensors.Find(id);
            if (tblATSensor == null)
            {
                return HttpNotFound();
            }
            return View(tblATSensor);
        }

        // POST: Sensors/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            tblATSensor tblATSensor = db.tblATSensors.Find(id);
            db.tblATSensors.Remove(tblATSensor);
            db.SaveChanges();
            return RedirectToAction("IndexSensors");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
