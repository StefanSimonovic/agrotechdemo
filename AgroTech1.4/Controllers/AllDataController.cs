﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using AgroTech1._4.Models;

namespace AgroTech1._4.Controllers
{
    public class AllDataController : Controller
    {
        private eMES_AgroTechEntities db = new eMES_AgroTechEntities();

        // GET: AllData
        public ActionResult IndexAllData()
        {
            return View(db.viATAllNodesDatas.ToList());
        }        

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
