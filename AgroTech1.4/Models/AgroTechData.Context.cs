﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AgroTech1._4.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class eMES_AgroTechEntities : DbContext
    {
        public eMES_AgroTechEntities()
            : base("name=eMES_AgroTechEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tblATNodeData> tblATNodeDatas { get; set; }
        public virtual DbSet<tblATNode> tblATNodes { get; set; }
        public virtual DbSet<tblATNodesSensorsConnection> tblATNodesSensorsConnections { get; set; }
        public virtual DbSet<tblATPlant> tblATPlants { get; set; }
        public virtual DbSet<tblATSensor> tblATSensors { get; set; }
        public virtual DbSet<viATAllNodesData> viATAllNodesDatas { get; set; }
        public virtual DbSet<viATNodesSensor> viATNodesSensors { get; set; }
    
        public virtual ObjectResult<sp_ATNodeDataSingleSensor_Result> sp_ATNodeDataSingleSensor(string node, string sensor)
        {
            var nodeParameter = node != null ?
                new ObjectParameter("node", node) :
                new ObjectParameter("node", typeof(string));
    
            var sensorParameter = sensor != null ?
                new ObjectParameter("sensor", sensor) :
                new ObjectParameter("sensor", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_ATNodeDataSingleSensor_Result>("sp_ATNodeDataSingleSensor", nodeParameter, sensorParameter);
        }
    }
}
